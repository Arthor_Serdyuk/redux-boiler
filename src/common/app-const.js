export const INITIAL_STATE = {
  users: {
    isLoading  : true,
    hasError   : false,
    usersList  : [],
    currentUser: {}
  }
};
