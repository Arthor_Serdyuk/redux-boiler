import { INITIAL_STATE } from '../../common/app-const';
import {
  GET_USERS_LIST_SUCCESS,
  FETCH_DATA_ERROR,
  GET_USER_SUCCESS,
  LOADING
} from '../actions/types';

const users = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_USERS_LIST_SUCCESS: {
      if (typeof (action.data) === 'object') {
        action.users = Object.keys(action.data).map(key => action.data[key]);
      }
      return { ...state, usersList: action.users };
    }

    case GET_USER_SUCCESS: {
      return { ...state, currentUser: action.data };
    }

    case FETCH_DATA_ERROR: {
      return { ...state, hasError: action.hasError };
    }

    case LOADING: {
      return { ...state, isLoading: action.isLoading };
    }

    default: {
      return state;
    }
  }
};

export default users;
