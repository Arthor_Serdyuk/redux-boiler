import { GET_USERS_LIST_SUCCESS } from '../actions/types';

const alertMiddleware = () => next => {
  return action => {
    if (action.type === GET_USERS_LIST_SUCCESS) {
      action.data.find(({id, name}) => {
        if (id === 5) alert(`User with id 5 - ${name}`);
      });
    }
    return next(action);
  };
};

export default alertMiddleware;
