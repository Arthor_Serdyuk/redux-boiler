import {
  GET_USERS_LIST_SUCCESS,
  FETCH_DATA_ERROR,
  GET_USER_SUCCESS,
  LOADING,
  FETCH_DATA
} from './types';

/**
 * cation to fetch the users data
 * @returns {Function} thunk
 */
export const getUsersList = () => ({
  type   : FETCH_DATA,
  payload: {
    url: {
      endpoint: 'https://jsonplaceholder.typicode.com/users/'
    },
    success: (data) => getUsersListSuccess(data),
    failure: (boolean) => fetchDataError(boolean),
    loader : (boolean) => loading(boolean)
  }
});

export const getUserById = (userId) => ({
  type   : FETCH_DATA,
  payload: {
    url: {
      endpoint: `https://jsonplaceholder.typicode.com/users/${userId}`
    },
    success: (data) => getUserSuccess(data),
    failure: (boolean) => fetchDataError(boolean),
    loader : (boolean) => loading(boolean)
  }
});

/**
 * action if there is an error
 * @param bool
 * @returns {{type, hasError: *}}
 */
export const fetchDataError = (bool) => ({
  type    : FETCH_DATA_ERROR,
  hasError: bool
});

/**
 * action when fetch is success
 * @param users
 * @returns {{type, users: *}}
 */
export const getUsersListSuccess = (users) => ({
  type: GET_USERS_LIST_SUCCESS,
  data: users
});

/**
 * action when fetch is success
 * @param user
 * @returns {{type, user: *}}
 */
export const getUserSuccess = (user) => ({
  type: GET_USER_SUCCESS,
  data: user
});

/**
 * action while loading
 * @param bool
 * @returns {{type, isLoading: *}}
 */
export const loading = (bool) => ({
  type     : LOADING,
  isLoading: bool
});