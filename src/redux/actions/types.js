// users
export const GET_USERS_LIST_SUCCESS = 'GET_USERS_LIST_SUCCESS';
export const GET_USER_SUCCESS = 'GET_USER_SUCCESS';
export const FETCH_DATA_ERROR = 'FETCH_DATA_ERROR';
export const LOADING = 'LOADING';
// api
export const FETCH_DATA = 'FETCH_DATA';
