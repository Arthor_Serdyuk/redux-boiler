import PropTypes from 'prop-types';
import React from 'react';

export default function UserDetails ({userDetails}) {
  return (
    <ul>
      <li>{userDetails.name}</li>
      <li>{userDetails.email}</li>
      <li>{userDetails.phone}</li>
      <li>{userDetails.username}</li>
      <li>{userDetails.website}</li>
      <li>{userDetails.name}</li>
    </ul>
  );
}

UserDetails.propTypes = {
  userDetails: PropTypes.object.isRequired
};
