import PropTypes from 'prop-types';
import React from 'react';
import {connect} from 'react-redux';
import { getUserById } from '../../redux/actions/users';
import { Loading } from '../common/Notifications';
import UserDetails from './UserDetails';

class DetailedUser extends React.Component {
  componentDidMount () {
    this.props.getUserById(parseInt(this.props.params.id));
  }

  render () {
    const {isLoading, currentUser} = this.props;
    return (
      <div>
        {
          isLoading
            ? <Loading />
            : <UserDetails userDetails={currentUser}/>
        }
      </div>
    );
  }
}

DetailedUser.propTypes = {
  isLoading  : PropTypes.bool.isRequired,
  currentUser: PropTypes.object.isRequired,
  params     : PropTypes.object.isRequired,
  getUserById: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  isLoading  : state.users.isLoading,
  currentUser: state.users.currentUser
});

const mapDispatchToProps = (dispatch) => ({
  getUserById: (userId) => dispatch(getUserById(userId))
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailedUser);
