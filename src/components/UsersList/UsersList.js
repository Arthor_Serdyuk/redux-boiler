import PropTypes from 'prop-types';
import React from 'react';
import User from './User';

export default function UsersList ({usersList}) {
  return (
    <div>
      {usersList.map(user => <User key={user.id} userData={user}/>)}
    </div>
  );
}

UsersList.propTypes = {
  usersList: PropTypes.array.isRequired
};
