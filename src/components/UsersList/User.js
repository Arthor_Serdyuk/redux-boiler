import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router';

export default function User ({userData}) {
  return (
    <div>
      <h1>{userData.name}</h1>
      <Link to={ `/${userData.id}` }>Go to user profile</Link>
    </div>
  );
}

User.propTypes = {
  userData: PropTypes.object.isRequired
};
