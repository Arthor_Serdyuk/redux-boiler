import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import {getUsersList} from '../redux/actions/users';
import { Loading } from './common/Notifications';
import UsersList from './UsersList/UsersList';

class App extends React.Component {
  componentDidMount () {
    this.props.getUsersList();
  }
  render () {
    const {usersList, isLoading} = this.props;
    return (
      <div>
        {this.props.children}
        {this.props.location.pathname === '/'
          ? (isLoading)
            ? <Loading />
            : <UsersList usersList={ usersList } />
          : null
        }
      </div>

    );
  }
}

App.propTypes = {
  isLoading   : PropTypes.bool.isRequired,
  children    : PropTypes.object.isRequired,
  location    : PropTypes.object.isRequired,
  usersList   : PropTypes.array.isRequired,
  getUsersList: PropTypes.func
};

const mapStateToProps = (state) => ({
  isLoading: state.users.isLoading,
  usersList: state.users.usersList
});

const mapDispatchToProps = (dispatch) => ({
  getUsersList: () => dispatch(getUsersList())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
