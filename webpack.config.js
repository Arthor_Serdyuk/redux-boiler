'use strict';

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const DynamicCdnWebpackPlugin = require('dynamic-cdn-webpack-plugin');
const BabiliPlugin = require('babili-webpack-plugin');
const CleanPlugin = require('./utils/clean-plugin');
const NodeUtils = require('./config/node-service');
const appConfig = require('./config/config');

const extractSass = new ExtractTextPlugin({
  filename : `bundle-v.${appConfig.version}.css`,
  allChunks: true,
  disable  : process.env.NODE_ENV === 'development'
});

const config = {
  devtool: 'cheap-module-source-map',
  output : {
    path      : path.join(__dirname, 'dist'),
    filename  : `bundle-v.${appConfig.version}.js`,
    publicPath: '/'
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.scss']
  },
  plugins: [
    new CleanPlugin({
      files: ['dist/*']
    }),
    extractSass,
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/index.html'),
      inject  : 'body'
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(
          process.env.NODE_ENV
        ),
        APP_CONFIG: JSON.stringify(
          appConfig
        )
      }
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name    : 'common',
      filename: `common-v.${appConfig.version}.js`,
      minChunks (module, count) {
        let context = module.context;
        return context && context.indexOf('node_modules') >= 0;
      }
    })
  ],
  module: {
    exprContextCritical: false, // Suppress "The request of a dependency is an expression"
    rules              : [
      {
        test   : /\.(js|jsx)$/,
        loaders: 'babel-loader',
        exclude: [/node_modules/]
      },
      {
        test: /\.scss$/,
        use : extractSass.extract({
          use: [
            {
              loader : 'css-loader',
              options: {
                modules       : true,
                importLoaders : 1,
                camelCase     : 'dashes',
                localIdentName: '[name]__[local]___[hash:base64:5]',
                minimize      : process.env.NODE_ENV === 'production',
                sourceMap     : true
              }
            }, {
              loader: 'postcss-loader'
            }, {
              loader : 'sass-loader',
              options: {
                includePaths: ['node_modules']
              }
            }
          ],
          fallback: 'style-loader'
        })
      },
      {
        test   : /\.css$/,
        loaders: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test   : /\.(eot|woff|woff2|ttf|png|jpg|svg)$/,
        loader : 'file-loader?limit=10000&name=[name]-[hash].[ext]',
        include: path.join(__dirname, 'src')
      },
      {
        test   : /\.json$/,
        loader : 'json-loader',
        include: path.join(__dirname, 'src')
      },
      {
        test  : /\.svg/,
        loader: 'svg-url-loader'
      }
    ]
  }
};

if (NodeUtils.isProduction()) {
  config.entry = './src/Bootstrap';
  config.plugins.push(
    new DynamicCdnWebpackPlugin(),
    new BabiliPlugin({
      mangle  : { 'topLevel': true },
      deadcode: true
    })
  );
} else {
  config.devtool = 'source-map';
  config.entry = [
    'react-hot-loader/patch',
    `webpack-dev-server/client?http://localhost:${appConfig.local.port}`,
    'webpack/hot/only-dev-server',
    './src/Bootstrap'
  ];
  config.plugins.push(
    new webpack.HotModuleReplacementPlugin()
  );
}
/* const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
config.plugins.push(
  new BundleAnalyzerPlugin({analyzerMode: 'static'})
); */

module.exports = config;
