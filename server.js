'use strict';

const config = require('./config/config');
const NodeService = require('./config/node-service');

const {local} = config;
if (!local) throw new Error('configuration cannot be null/undefined');

const PORT = local.port;

if (NodeService.isProduction()) {
  const engine = require('consolidate');
  // start express app
  const express = require('express');
  const path = require('path');

  const app = express();

  app.engine('html', engine.mustache);
  app.set('view engine', 'html');

  // Configure static resources
  app.use(
    express.static(
      path.join(__dirname, '/dist'),
      { maxAge: 86400000 * 30 } // 30 days
    )
  );

  // Configure server-side routing
  app.get('*', (req, res) => {
    // serve file
    const dist = path.join(
      __dirname, '/dist/index.html'
    );
    res.render(dist, {country: req.headers['cf-ipcountry']});
  });

  // Start the server
  const server = app.listen(process.env.PORT || 8081, () => {
    const port = server.address().port;
    console.log(`App listening on port ${port}`);
  });
} else {
  // start dev server
  const webpack = require('webpack');
  const WebpackDevServer = require('webpack-dev-server');
  const config = require('./webpack.config.js');

  new WebpackDevServer(webpack(config), {
    hot               : true,
    historyApiFallback: true
  }).listen(PORT, 'localhost', error => {
    console.log(error || `Started WebpackDevServer on port ${PORT}`);
  });
}
